package com.health.insurance.bean;

import java.util.Scanner;

public class HealthInsurancePlan 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		HealthBean hb=new HealthBean();
		
		System.out.println("Name: ");
		hb.setName(sc.nextLine());
		
		System.out.println("Gender: ");
		hb.setGender(sc.nextLine());
		
		System.out.println("Age in years: ");
		hb.setAge(sc.nextInt());
		
		System.out.println("Current Health:");
		
		System.out.println("Hypertension (Yes/No): ");
		hb.setHypertension(sc.next());
		
		System.out.println("Blood pressure (Yes/No): ");
		hb.setBp(sc.next());
		
		System.out.println("Blood Suger (Yes/No) :");
		hb.setBs(sc.next());
		
		System.out.println("Overwieght (Yes/No): ");
		hb.setOverweight(sc.next());
		
		System.out.println("Habits:");
		System.out.println("Smoking (Yes/No): ");
		hb.setSmoking(sc.next());
		System.out.println("Alcohol (Yes/No): ");
		hb.setAlcohol(sc.next());
		System.out.println("Daily exercise (Yes/No) :");
		hb.setExercise(sc.next());
		System.out.println("Drugs (Yes/No): ");
		hb.setDrugs(sc.next());
		
		
		System.out.println("Health Insurance Premium for Mr."+hb.getName()+": Rs."+calculateHealthPlan(hb));
	}

	private static double calculateHealthPlan(HealthBean hb) 
	{
		double basePremium=5000,reducePremium = 5000;
		if(18<hb.getAge()&&hb.getAge()<40)
		{
			basePremium=basePremium+(basePremium*0.1);
			
			if(((25<hb.getAge()&&hb.getAge()<40)))
			{
				basePremium=basePremium+(basePremium*0.1);
			}
			if(((30<hb.getAge()&&hb.getAge()<40)))
			{
				basePremium=basePremium+(basePremium*0.1);
			}
			if((35<hb.getAge()&&hb.getAge()<40))
			{
				basePremium=basePremium+(basePremium*0.1);
			}
		}
		else if(40<hb.getAge())
		{
			basePremium=basePremium+(basePremium*0.2);
		}
		
		if(hb.getGender().equalsIgnoreCase("male"))
		{
			basePremium=basePremium+(basePremium*0.02);
		}
		
		if(hb.getHypertension().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getBp().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getBs().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		if(hb.getOverweight().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.01);
		}
		
		//good Habits
		
		if(hb.getExercise().equalsIgnoreCase("yes"))
		{
			reducePremium=basePremium-(basePremium*0.03);
		}
		
		//bad habits
		if(hb.getSmoking().equalsIgnoreCase("yes"))
		{
			basePremium=reducePremium+(basePremium*0.03);
			
		}
		if(hb.getAlcohol().equalsIgnoreCase("yes"))
		{
			basePremium=reducePremium+(basePremium*0.03);
		}
		if(hb.getDrugs().equalsIgnoreCase("yes"))
		{
			basePremium=basePremium+(basePremium*0.03);
		}
		
		return round(basePremium,2);
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

}
